type expr =
  | EConst of int
  | EAdd of expr * expr
  [@@deriving yojson]
;;
  
print_endline (Yojson.Safe.to_string
                 (expr_to_yojson (EAdd (EConst 73, EConst 42))));;
